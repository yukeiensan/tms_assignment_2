#!/usr/bin/env python3

import plotly.graph_objects as go
import argparse


def parsing_arguments():
    parser = argparse.ArgumentParser(description="Draw chart from CSV file")
    parser.add_argument('file', help='CSV file containing data')
    args = parser.parse_args()

    return args


def check_data_structure(structure):
    expected_structure = ["price", "quantity demanded", "quantity supplied"]

    if len(expected_structure) != len(structure):
        print("Wrong csv file header structure")
        exit(84)

    for index, value in enumerate(expected_structure):
        if value != structure[index]:
            print("Wrong csv file header structure")
            exit(84)


def csv_to_data(file):
    f = open(file, "r+")
    structure = f.readline().strip('\n').split(", ")
    data_list = []

    check_data_structure(structure)
    for line in f:
        data = {}
        raw_data = line.strip('\n').split(',')
        for index, name in enumerate(structure):
            data[name] = raw_data[index]
        data_list.append(data)

    return data_list


def parse_data(data_list):
    price = []
    demand = []
    supply = []

    for data in data_list:
        price.append(float(data["price"]))
        demand.append(float(data["quantity demanded"]))
        supply.append(float(data["quantity supplied"]))

    return price, demand, supply


def draw_chart(data_list, filename):
    fig = go.Figure()
    fig.update_layout(title="Demand Supply graph based on " + filename + " file",
                      xaxis_title='Price',
                      yaxis_title='Quantity')
    price, demand, supply = parse_data(data_list)

    fig.add_trace(go.Scatter(x=price, y=demand, name="Demand Line", line=dict(color='blue', width=2)))
    fig.add_trace(go.Scatter(x=price, y=supply, name="Supply Line", line=dict(color='orange', width=2)))
    fig.show()


def main():
    args = parsing_arguments()
    data_list = csv_to_data(args.file)
    draw_chart(data_list, args.file)


if __name__ == "__main__":
    main()