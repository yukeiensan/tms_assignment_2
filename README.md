# Requirements
`pip install plotly`

# Usage
usage: supply_demand.exe [-h] file

Draw chart from CSV file

positional arguments:
  file        CSV file containing data

optional arguments:
  -h, --help  show this help message and exit